import {
  Controller,
  Post,
  Body,
  UseGuards,
  Request,
  Get,
} from '@nestjs/common';
import { UserService } from './user.service';
import { User, UserLogin } from './user.model';
import { Country } from '../country/country.model';
import { AuthService } from '../auth/auth.service';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { ModelType } from '@typegoose/typegoose/lib/types';
import { InjectModel } from 'nestjs-typegoose';
import mongoose from 'mongoose';
import { ApiBearerAuth } from '@nestjs/swagger';

@Controller('users')
export class UserController {
  constructor(
    private userService: UserService,
    private authService: AuthService,
    @InjectModel(User) private readonly UserModel: ModelType<User>,
    @InjectModel(Country) private readonly CountryModel: ModelType<Country>,
  ) {}

  @Post('register')
  async register(@Body() userData: User): Promise<User> {
    try {
      const newReq: any = { ...userData };
      const { country, password, email } = newReq;

      const countryData = await this.CountryModel.findOne(
        new mongoose.Types.ObjectId(country),
      ).exec();

      if (!countryData) throw new Error(`Country with ID ${country} not found`);

      // const hashedPassword = await bcrypt.hash(password, 10);

      const newUser = new this.UserModel({
        email,
        password,
        country: new mongoose.Types.ObjectId(country),
      });

      return newUser.save();
    } catch (error) {
      return error.message;
    }
  }

  @Post('login')
  async login(@Body() userDto: UserLogin): Promise<{ token: string }> {
    const user: any = await this.userService.findByCredentials(
      userDto.email,
      userDto.password,
    );
    const token = await this.authService.login(user);
    return { token };
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Get()
  async getAllUsers(@Request() req): Promise<User[]> {
    return this.userService.getAllUsers();
  }
}
