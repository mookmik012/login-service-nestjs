import { prop, Ref } from '@typegoose/typegoose';
import { Country } from '../country/country.model';
import { ApiProperty } from '@nestjs/swagger';

export class User {
  @ApiProperty({
    example: 'john@gmail.com',
    description: 'The email of user',
  })
  @prop({ required: true, unique: true })
  email: string;

  @ApiProperty({
    example: '1234',
    description: 'The password of user',
  })
  @prop({ required: true })
  password: string;

  @ApiProperty({
    example: '6499b7d29a9804ffa832d69f',
    description: 'ObjectId country of user in string type',
  })
  @prop({ ref: Country, required: true })
  country: Ref<Country>;
}

export class UserLogin {
  @ApiProperty({
    example: 'john@gmail.com',
    description: 'The email of user',
  })
  @prop({ required: true, unique: true })
  email: string;

  @ApiProperty({
    example: '1234',
    description: 'The password of user',
  })
  @prop({ required: true })
  password: string;
}
