import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserService } from '../user/user.service';
import { User } from '../user/user.model';
import { JwtPayload } from './jwt-payload.interface';
import { mongoose } from '@typegoose/typegoose';

type WithObjectId<T> = T & { _id: mongoose.Types.ObjectId };

@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private jwtService: JwtService,
  ) {}

  async validateUser(payload: JwtPayload): Promise<User> {
    return this.userService.findById(payload.userId);
  }

  async login(user: WithObjectId<User>): Promise<string> {
    const payload: JwtPayload = { userId: user._id.toString() };
    return this.jwtService.sign(payload, { secret: 'SECRETKEY' });
  }
}
