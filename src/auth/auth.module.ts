import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { JwtStrategy } from './jwt.strategy';
import { AuthService } from './auth.service';
import { UserModule } from '../user/user.module';
import { UserService } from 'src/user/user.service';
import { TypegooseModule } from 'nestjs-typegoose';
import { User } from '../user/user.model';

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secret: 'SECRETKEY',
      signOptions: { expiresIn: '1h' },
    }),
    UserModule,
    TypegooseModule.forFeature([User]),
  ],
  providers: [AuthService, JwtStrategy, UserService],
  exports: [AuthService],
})
export class AuthModule {}
