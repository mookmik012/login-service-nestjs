import { Controller, Get, Post, Body } from '@nestjs/common';
import { InjectModel } from 'nestjs-typegoose';
import { ModelType } from '@typegoose/typegoose/lib/types';
import { Country } from './country.model';

@Controller('countries')
export class CountryController {
  constructor(
    @InjectModel(Country) private readonly countryModel: ModelType<Country>,
  ) {}

  @Get()
  async getAllCountries(): Promise<Country[]> {
    return this.countryModel.find().exec();
  }

  @Post()
  async createCountry(@Body() countryDto: Country): Promise<Country> {
    const createdCountry = new this.countryModel(countryDto);
    return createdCountry.save();
  }
}
