import { Module } from '@nestjs/common';
import { TypegooseModule } from 'nestjs-typegoose';
import { CountryController } from './country.controller';
import { Country } from './country.model';

@Module({
  imports: [TypegooseModule.forFeature([Country])],
  controllers: [CountryController],
})
export class CountryModule {}
