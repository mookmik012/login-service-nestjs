import { prop } from '@typegoose/typegoose';
import { ApiProperty } from '@nestjs/swagger';

export class Country {
  @ApiProperty({
    example: 'Cambodia',
    description: 'The name of country',
  })
  @prop({ required: true })
  name: string;

  @ApiProperty({
    example: 'cb',
    description: 'The code of country',
  })
  @prop({ required: true })
  code: string;
}
