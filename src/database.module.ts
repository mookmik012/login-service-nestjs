import { Module } from '@nestjs/common';
import { TypegooseModule } from 'nestjs-typegoose';
import databaseConfig from '../config/database.config';

@Module({
  imports: [TypegooseModule.forRoot(databaseConfig.uri)],
})
export class DatabaseModule {}
